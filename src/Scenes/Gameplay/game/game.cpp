#include "game.h"

namespace arca {

	namespace game {

		using namespace game_objects;

		Ball ball[MAX_BALLS];
		Pad pad;
		Game_area game_a;
		Score_area score_a;

		Texture2D padTX;
		Texture2D background;
		Texture2D ballTX;

		Vector2 btnMenuTXPos;
		Texture2D btnMenuTX;
		Text btnMenuTx;

		Vector2 btnPauseTXPos;
		Texture2D btnPauseTX;
		Text btnPauseTx;

		Button btnMenu;
		Button btnPause;

		Rectangle limLeft;
		Rectangle limUp;
		Rectangle limRight;
		Rectangle limDown;
		Rectangle limMid;

		Texture2D brickTX[6];

		int lives;
		int level;
		int score;

		bool pause = false;
		bool lose = false;

		void init() {

			ball[0].visible = true;
			ball[0].pos.x = pad.pos.x + pad.width / 2;
			ball[0].pos.y = pad.pos.y - pad.height / 2 - ball[0].rad - 2;
			float aux = screen::size[static_cast<int>(screen::WindowSize::SMALL)].x * screen::size[static_cast<int>(screen::WindowSize::SMALL)].y;
			ball[0].rad = (screen::SCREEN_HEIGHT * screen::SCREEN_WIDTH) * ball[0].min_rad / aux;
			ball[0].stopped = true;

			for (short i = 0; i < MAX_BALLS; i++) {
				ball[i].color = {
					static_cast<unsigned char>(GetRandomValue(100,200)),
					static_cast<unsigned char>(GetRandomValue(100,200)),
					static_cast<unsigned char>(GetRandomValue(100,200)),
					255
				};
			}

			padTX = LoadTexture("../res/assets/pad/pad.png");
			padTX.height = pad.height;
			padTX.width = pad.width;

			background = LoadTexture("../res/assets/game/BG0.png");
			background.height = game_a.HEIGHT;
			background.width = game_a.WIDTH;

			limLeft = { 0, 0, screen::SCREEN_WIDTH / 20, screen::SCREEN_HEIGHT };
			limUp = { 0, 0, screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT / 20 };
			limRight = { screen::SCREEN_WIDTH * 19 / 20, 0, screen::SCREEN_WIDTH / 20, screen::SCREEN_HEIGHT };
			limMid = { 0, screen::SCREEN_HEIGHT * 14 / 20, screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT / 20 };
			limDown = { 0, screen::SCREEN_HEIGHT * 19 / 20, screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT / 20 };

			btnPause.size = { screen::SCREEN_WIDTH * 28 / 40, 0, screen::SCREEN_WIDTH * 7 / 40, screen::SCREEN_HEIGHT * 4 / 80 };
			btnMenu.size = { screen::SCREEN_WIDTH * 28 / 40, 0, screen::SCREEN_WIDTH * 7 / 40, screen::SCREEN_HEIGHT * 4 / 80 };

			btnPauseTX = LoadTexture("../res/assets/button/log0.png");
			btnPauseTXPos.x = screen::SCREEN_WIDTH * 26 / 40;
			btnPauseTXPos.y = screen::SCREEN_HEIGHT * 61 / 80;
			btnPauseTX.height = screen::SCREEN_HEIGHT * 3 / 40;
			btnPauseTX.width = screen::SCREEN_WIDTH * 11 / 40;
			btnPauseTx.size = screen::SCREEN_HEIGHT * 3 / 80;
			btnPauseTx.pos.x = btnPauseTXPos.x + btnPauseTX.width / 2 - MeasureText("PAUSE", btnPauseTx.size) / 2;
			btnPauseTx.pos.y = btnPauseTXPos.y + btnPauseTX.height / 2 - btnPauseTx.size / 2;

			btnMenuTX = LoadTexture("../res/assets/button/log0.png");
			btnMenuTXPos.x = screen::SCREEN_WIDTH * 26 / 40;
			btnMenuTXPos.y = screen::SCREEN_HEIGHT * 68 / 80;
			btnMenuTX.height = screen::SCREEN_HEIGHT * 3 / 40;
			btnMenuTX.width = screen::SCREEN_WIDTH * 11 / 40;
			btnMenuTx.size = screen::SCREEN_HEIGHT * 3 / 80;
			btnMenuTx.pos.x = btnMenuTXPos.x + btnMenuTX.width / 2 - MeasureText("MENU", btnMenuTx.size) / 2;
			btnMenuTx.pos.y = btnMenuTXPos.y + btnMenuTX.height / 2 - btnMenuTx.size / 2;

			btnPause.size.y = btnPauseTXPos.y + btnPauseTX.height / 2 - btnPause.size.height / 2;
			btnMenu.size.y = btnMenuTXPos.y + btnMenuTX.height / 2 - btnMenu.size.height / 2;

			ballTX = LoadTexture("../res/assets/ball/ball.png");
			ballTX.height = ball[0].rad * 3.5f;
			ballTX.width = ball[0].rad * 2.5f;

			for (short i = 0; i < 6; i++)
			{

				brickTX[i] = LoadTexture(&(textureAssigner(i))[0]); 
				brickTX[i].height = screen::SCREEN_HEIGHT * 1 / 30;
				brickTX[i].width = screen::SCREEN_WIDTH * 12 / 80;
			}

			levelCreator();

			for (short i = 0; i < MAX_BLOCKS_Y; i++)
			{
				for (short j = 0; j < MAX_BLOCKS_X; j++)
				{

					brick1[i][j].pos.x = screen::SCREEN_WIDTH * 2 / 20 + i * brickTX[0].width + i * screen::SCREEN_WIDTH/80;
					brick1[i][j].pos.y = screen::SCREEN_HEIGHT / 20 + j * brickTX[0].height + 2;

				}
			}

			level = 1;
			lives = 3;
			score = 0;

		}

		void update() {

			Vector2 mousePoint = GetMousePosition();

			pad.pos.x = mousePoint.x - pad.width / 2; //movimiento pad

			if (!pause) {

				if (ball[0].stopped) {

					ball[0].pos.x = mousePoint.x;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						ball[0].stopped = false;

					}

				}

				else {

					ball[0].pos.x += ball[0].dirX * ball[0].speed;
					ball[0].pos.y += ball[0].dirY * ball[0].speed;

					for (short i = 0; i < MAX_BALLS; i++) { //con los limites

						if (CheckCollisionCircleRec(ball[i].pos, ball[i].rad, limRight)) {
							ball[i].dirX *= -1;
						}
						if (CheckCollisionCircleRec(ball[i].pos, ball[i].rad, limLeft)) {
							ball[i].dirX *= -1;
						}
						if (CheckCollisionCircleRec(ball[i].pos, ball[i].rad, limUp)) {
							ball[i].dirY *= -1;
						}
						if (CheckCollisionCircleRec(ball[i].pos, ball[i].rad, limMid)) {
							lives--;
						}

					}

					for (short i = 0; i < MAX_BALLS; i++) {

						if (CheckCollisionCircleRec(ball[i].pos, ball[i].rad, { pad.pos.x,pad.pos.y,pad.width,pad.height })) {

							if (ball[i].dirY > 0) {

								if (ball[i].dirX > 0) {
									if (ball[i].pos.x < pad.pos.x + pad.width / 2) {
										ball[i].dirX *= -1;
									}
								}
								else {
									if (ball[i].pos.x > pad.pos.x + pad.width / 2) {
										ball[i].dirX *= -1;
									}
								}

								ball[i].dirY *= -1;

							}

							ball[i].speed += 0.005f;

						}
					}

					

					for (short z = 0; z < MAX_BALLS; z++) {

						for (short j = 0; j < MAX_BLOCKS_Y; j++)
						{
							for (short i = 0; i < MAX_BLOCKS_X; i++)
							{
								Rectangle brk = { brick1[i][j].pos.x, brick1[i][j].pos.y, brick1[i][j].TX.width, brick1[i][j].TX.height };

								if (CheckCollisionCircleRec(ball[z].pos, ball[z].rad, brk)) {

									if (ball[z].dirY != 0) {
										ball[z].dirY *= -1;
									}
									else if (ball[z].dirX != 0) {
										ball[z].dirX *= -1;
									}	

									brick1[i][j].type = BlockType::NONE;
									brick1[i][j].pos.x = -100;
									brick1[i][j].pos.y = -100;
									score += 10;

								}

							}

						}


					}


				}

			}

			//----------------------------------------------------------------------------------

			if (CheckCollisionPointRec(mousePoint, btnMenu.size)) {

				btnMenu.tx.color = LIME;

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					using namespace config;

					config::nextScene = static_cast<Scenes>(Scenes::MENU);

				}

			}
			else {

				btnMenu.tx.color = WHITE;

			}

			if (CheckCollisionPointRec(mousePoint, btnPause.size)) {

				if (!pause)
					btnPause.tx.color = LIME;
				else
					btnPause.tx.color = RED;

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					pause = !pause;

				}

			}
			else {

				if (!pause)
					btnPause.tx.color = WHITE;
				else
					btnPause.tx.color = LIME;

			}


		}

		void draw() {

			BeginDrawing();

			ClearBackground(RAYWHITE);

			DrawTexture(background, game_a.MIN.x, game_a.MIN.y, WHITE);//background
			DrawRectangle(score_a.MIN.x, score_a.MIN.y, score_a.WIDTH, score_a.HEIGHT, score_a.color); // score area
			DrawTexture(padTX, pad.pos.x, pad.pos.y, WHITE);

			for (short i = 0; i < MAX_BALLS; i++) {

				if (ball[i].visible) {

					DrawTexture(ballTX, ball[i].pos.x - ballTX.width / 2, ball[i].pos.y - ballTX.height / 2, ball[i].color);

				}

			}

			DrawRectangleRec(limDown, BROWN);
			DrawRectangleRec(limMid, BROWN);
			DrawRectangleRec(limUp, BROWN);
			DrawRectangleRec(limLeft, DARKBROWN);
			DrawRectangleRec(limRight, DARKBROWN);

			for (short i = 0; i < MAX_BLOCKS_Y; i++)
			{
				for (short j = 0; j < MAX_BLOCKS_X; j++)
				{
					if(brick1[i][j].type!=BlockType::NONE)
					DrawTexture(brick1[i][j].TX, brick1[i][j].pos.x, brick1[i][j].pos.y, WHITE);

				}
			}

			DrawTexture(btnMenuTX, btnMenuTXPos.x, btnMenuTXPos.y, WHITE);
			DrawTexture(btnPauseTX, btnPauseTXPos.x, btnPauseTXPos.y, WHITE);

			DrawText("SCORE", screen::SCREEN_WIDTH * 2 / 20, btnPause.size.y, btnPauseTx.size, RED);
			DrawText("LIVES", screen::SCREEN_WIDTH * 2 / 20, btnMenu.size.y, btnMenuTx.size, RED);
			for (short i = 0; i < lives - 1; i++)
			{
				DrawTexture(ballTX, screen::SCREEN_WIDTH * 2 / 20 + MeasureText("LIVES  ", btnMenuTx.size) + ballTX.width * i, btnMenu.size.y - 5, WHITE);
			}

			//DrawCircle(ball[0].pos.x, ball[0].pos.y, ball[0].rad, ball[0].color);
			DrawText("PAUSE", btnPauseTx.pos.x, btnPauseTx.pos.y, btnPauseTx.size, btnPause.tx.color);
			DrawText("MENU", btnMenuTx.pos.x, btnMenuTx.pos.y, btnMenuTx.size, btnMenu.tx.color);

			EndDrawing();

		}

		void deinit() {

			UnloadTexture(background);
			UnloadTexture(btnPauseTX);
			UnloadTexture(btnMenuTX);
			UnloadTexture(padTX);
			UnloadTexture(ballTX);

		}

		void levelCreator() {


			for (short j = 0; j < MAX_BLOCKS_Y; j++) {

				for (short i = 0; i < MAX_BLOCKS_X; i++) {

					if (j % 2 == 1) {

						if (i % 2 == 1) {

							brick1[i][j].type = BlockType::BREAKEABLE;
							brick1[i][j].TX = brickTX[GetRandomValue(0, 5)];
						}

					}
					else {

						if (i % 2 == 0) {

							brick1[i][j].type = BlockType::BREAKEABLE;
							brick1[i][j].TX = brickTX[GetRandomValue(0, 5)];

						}

					}

				}
			}


			for (short j = 0; j < MAX_BLOCKS_X; j++) {

				for (short i = 0; i < MAX_BLOCKS_Y; i++) {



				}

			}

			for (short j = 0; j < MAX_BLOCKS_X; j++) {

				for (short i = 0; i < MAX_BLOCKS_Y; i++) {



				}

			}

		}
	

		string textureAssigner(int i) {

			string opt;
			switch (i) {

				case 0:
					opt = "../res/assets/bricks/B0.png";
					break;
				case 1:
					opt = "../res/assets/bricks/B1.png";
					break;
				case 2:
					opt = "../res/assets/bricks/B2.png";
					break;
				case 3:
					opt = "../res/assets/bricks/B3.png";
					break;
				case 4:
					opt = "../res/assets/bricks/B4.png";
					break;
				case 5:
					opt = "../res/assets/bricks/B5.png";
					break;

			}

			return opt;
		}

	}

}