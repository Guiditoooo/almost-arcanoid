#pragma once
#include "raylib.h"
#include "Extern Vars/Externs.h"

using namespace std;

namespace arca {

	namespace game {

		using namespace game_objects;
		using namespace blocks;

		const int MAX_BALLS = 10;

		struct Button {

			Rectangle size;
			Text tx;
			bool selected = false;
			Texture2D TX;

		};

		extern Button button;

		extern Ball ball[MAX_BALLS];
		extern Pad pad;
		extern Game_area game_a;
		extern Score_area score_a;

		extern Button btnMenu;
		extern Button btnPause;

		extern Texture2D padTX;
		extern Texture2D background;
		extern Texture2D ballTX;

		extern Vector2 btnMenuTXPos;
		extern Texture2D btnMenuTX;
		extern Text btnMenuTx;
		extern Vector2 btnPauseTXPos;
		extern Texture2D btnPauseTX;
		extern Text btnPauseTx;

		extern Rectangle limLeft;
		extern Rectangle limUp;
		extern Rectangle limRight;
		extern Rectangle limDown;
		extern Rectangle limMid;

		extern Texture2D brickTX[6];

		extern int lives;
		extern int level;
		extern int score;

		extern bool pause;
		extern bool lose;

		void init();
		void update();
		void draw();
		void deinit();
		void levelCreator();
		string textureAssigner(int);

	}

}