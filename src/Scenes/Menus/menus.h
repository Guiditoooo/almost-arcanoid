#pragma once
#include "raylib.h"
#include "Extern Vars/Externs.h"
#include <string>

using namespace std;

namespace arca {

	namespace config {

		namespace menu {


			const int baseTextHeight = 30; // 30px
			const int howManyButtons = 4;

			enum class menuOptions { PLAY = 0, OPTIONS, CREDITS, EXIT, NONE };

			struct Button {

				string text;
				Color color = BLACK;
				Rectangle square;
				menuOptions id;
				bool selected = false;

			};

			extern Button buttons[howManyButtons];
			extern Texture2D background;
			extern Texture2D title;
			extern Vector2 ttPos;

			extern bool continueInGame;

			extern void init();
			extern void update();
			extern void draw();
			extern void deinit();

		}

	}

}