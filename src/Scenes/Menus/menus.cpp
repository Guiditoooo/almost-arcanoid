#include "menus.h"
#include <iostream>
#include <string>

namespace arca {

	namespace config {

		namespace menu {

			bool continueInGame = true;
		
			Button buttons[howManyButtons];
			Texture2D background;
			Texture2D title;
			int hoveredOption = 0;
			int hoveredOptionPast = 0;

			Vector2 ttPos;

			void init() {
				
				background = LoadTexture("../res/assets/menu/sprite_0.png");
				background.height = screen::SCREEN_HEIGHT;
				background.width = screen::SCREEN_WIDTH;

				title = LoadTexture("../res/assets/title/title2.png");
				title.height = screen::SCREEN_HEIGHT*3/10;
				title.width = screen::SCREEN_WIDTH * 17/20;
				ttPos.x = screen::SCREEN_WIDTH / 2 - title.width / 2;
				ttPos.y = screen::SCREEN_HEIGHT / 20;
				
				for (short i = 0 ; i < howManyButtons ; i++) {

					Button btn;
					string text;

					switch ((menuOptions)i)	{

						case arca::config::menu::menuOptions::PLAY:
							text = "PLAY";
							break;
						case arca::config::menu::menuOptions::OPTIONS:
							text = "CONFIG";
							break;
						case arca::config::menu::menuOptions::CREDITS:
							text = "CREDITS";
							break;
						case arca::config::menu::menuOptions::EXIT:
							text = "EXIT";
							break;

					}

					btn.text = text;
					btn.id = (menuOptions)i;

					btn.square.width = (float)MeasureText(&btn.text[0], baseTextHeight);
					btn.square.height = (float)baseTextHeight;
					btn.square.x = (float)(screen::SCREEN_WIDTH / 2 - MeasureText(&btn.text[0], btn.square.height) / 2);
					btn.square.y = screen::SCREEN_HEIGHT * 17 / 40 + screen::SCREEN_HEIGHT * i * 2 / 25;

					btn.color = BLACK;

					buttons[i] = btn;

				}
				int a = 1;
			}

			void update() {

				Vector2 mousePoint = GetMousePosition();

				for (int i = 0; i < howManyButtons; i++) {

					if (CheckCollisionPointRec(mousePoint, buttons[i].square)) {
						buttons[i].selected = true;
						if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
							config::nextScene = static_cast<Scenes>(i+1);
						}
						else {
							config::nextScene = Scenes::MENU;
						}
					}
					else {
						buttons[i].selected = false;
					}

				}

			}

			void draw() {

				const float plusSelected = 1.15f;

				Color color;

				BeginDrawing();

				ClearBackground(RAYWHITE);

				DrawTextureEx(background, { 0,0 }, 0, 1, WHITE);
				DrawTextureEx(title, ttPos, 0, 1, WHITE);
				

				for (short i = 0; i < howManyButtons; i++) {

					Button btn = buttons[i];
					int heightCorrect = btn.square.height;

					if (btn.selected) {
						color = LIME;
						btn.square.height *= plusSelected;
						btn.square.width = MeasureText(&btn.text[0], baseTextHeight);
						btn.square.x = screen::SCREEN_WIDTH / 2 - MeasureText(&btn.text[0], btn.square.height) / 2;
						btn.square.y = screen::SCREEN_HEIGHT * 17 / 40 + screen::SCREEN_HEIGHT * i * 2 / 25 - (btn.square.height - heightCorrect)/2;
						//                      BASE                   +                VARIACION           - DIFERENCIA ENTRE LETRA GRANDE Y CHICA
					}
					else {
						color = btn.color;
					}

					DrawText(&(btn.text)[0], btn.square.x, btn.square.y, btn.square.height, color);

				}

				EndDrawing();

			}

			void deinit() {
				
				UnloadTexture(background);

			}

		}

	}

}