﻿#include "credits.h"

namespace arca {

	namespace config {

		namespace credits {

			Button button;

			Texture2D logo;

			AudioSelector index;
			CreditIndex cIndex;

			Text artist;
			Text lib;
			Text dev;
			Text musicA[HowManyLvls];
			Text categ[4];

			Color color;

			void init() {

				logo = LoadTexture("../res/assets/logo/RayLib.png");
				logo.height = 100;
				logo.width = 100;

				button.tx.size = screen::SCREEN_HEIGHT * 3 / 80;
				button.size.height = screen::SCREEN_HEIGHT * 3 / 40;
				button.size.width = screen::SCREEN_WIDTH * 11 / 40;
				button.size.x = screen::SCREEN_WIDTH * 3 / 4 - MeasureText("MENU", button.tx.size) / 2;
				button.size.y = screen::SCREEN_HEIGHT*9/10 - button.size.height / 2;
				
				button.tx.pos.x = button.size.x + button.size.width / 2 - (float)MeasureText("MENU", button.tx.size)/2;
				button.tx.pos.y = button.size.y + button.size.height / 2 - button.tx.size/2;

				button.TX = LoadTexture("../res/assets/button/log0.png");
				button.TX.width = button.size.width;
				button.TX.height = button.size.height;

				button.color = WHITE;

				color =	  { static_cast<unsigned char>(GetRandomValue(0,100)),
							static_cast<unsigned char>(GetRandomValue(0,100)),
							static_cast<unsigned char>(GetRandomValue(0,100)),
							200
				};
				
				artist.tx = "Frncisco Rodriguez";
				artist.size = screen::SCREEN_HEIGHT * 3 / 8;

				lib.tx = "RayLib";
				lib.size = screen::SCREEN_HEIGHT * 3 / 8;

				dev.tx = "Guido Tello";
				dev.size = screen::SCREEN_HEIGHT * 3 / 8;

				musicA[static_cast<int>(AudioSelector::MENU)].tx = "-Menu Music By: 'After The Fall'\n- Track Name: 'Dark Sun'\n- ATF LinkTree - linktr.ee/AftertheFall\n- DOWNLOAD @ www.chilloutmedia.com/atf\n- License: Creative Commons Attribution - ShareAlike 4.0 International(CC BY - SA 4.0)\n- - Full license here: creativecommons.org/licenses/\n- Music released by: Chill Out Records @ https ://goo.gl/fh3rEJ";
				musicA[static_cast<int>(AudioSelector::MENU)].size = screen::SCREEN_HEIGHT * 3 / 8;

				musicA[static_cast<int>(AudioSelector::LVL1)].tx = "-LVL1 Music By: 'After The Fall'\n- Track Name: 'Vibe Check'\n- ATF LinkTree - linktr.ee/AftertheFall\n- DOWNLOAD @ www.chilloutmedia.com/atf \n- License: Creative Commons Attribution - ShareAlike 4.0 International(CC BY - SA 4.0) \n- - Full license here: creativecommons.org/licenses \n- Music released by: Chill Out Records @ https ://goo.gl/fh3rEJ";
				musicA[static_cast<int>(AudioSelector::LVL2)].size = screen::SCREEN_HEIGHT * 3 / 8;

				
				categ[static_cast<int>(CreditIndex::AUDIO)].tx = "Audio";
				categ[static_cast<int>(CreditIndex::AUDIO)].size = screen::SCREEN_HEIGHT / 20;
				categ[static_cast<int>(CreditIndex::AUDIO)].pos.x = screen::SCREEN_WIDTH * 2 / 20;
				categ[static_cast<int>(CreditIndex::AUDIO)].pos.y = screen::SCREEN_HEIGHT * 2 / 20;

				categ[static_cast<int>(CreditIndex::ARTIST)].tx = "Artist: Francisco Rodriguez";
				categ[static_cast<int>(CreditIndex::ARTIST)].size = screen::SCREEN_HEIGHT / 20;
				categ[static_cast<int>(CreditIndex::ARTIST)].pos.x = screen::SCREEN_WIDTH * 2 / 20;
				categ[static_cast<int>(CreditIndex::ARTIST)].pos.y = screen::SCREEN_HEIGHT * 7 / 20;

				categ[static_cast<int>(CreditIndex::LIBRARY)].tx = "Library: RayLib";
				categ[static_cast<int>(CreditIndex::LIBRARY)].size = screen::SCREEN_HEIGHT / 20;
				categ[static_cast<int>(CreditIndex::LIBRARY)].pos.x = screen::SCREEN_WIDTH * 2 / 20;
				categ[static_cast<int>(CreditIndex::LIBRARY)].pos.y = screen::SCREEN_HEIGHT * 9 / 20;

				categ[static_cast<int>(CreditIndex::DEVELOPER)].tx = "Developer: Guido Tello";
				categ[static_cast<int>(CreditIndex::DEVELOPER)].size = screen::SCREEN_HEIGHT / 20;
				categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.x = screen::SCREEN_WIDTH * 2 / 20;
				categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.y = screen::SCREEN_HEIGHT * 11 / 20;


			}

			

			void update() {

				Vector2 mousePoint = GetMousePosition();

				if (CheckCollisionPointRec(mousePoint,button.size)) {

					button.color = LIME;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						config::nextScene = static_cast<Scenes>(Scenes::MENU);

					}

				}
				else {

					button.color = WHITE;

				}

			}

			void draw() {

				BeginDrawing();

				ClearBackground(BLACK);

				DrawRectangle(0, 0, screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT, color);//BG

				DrawText("CREDITS", screen::SCREEN_WIDTH / 2 - MeasureText("CREDITS", screen::SCREEN_HEIGHT * 3 / 20) / 2, screen::SCREEN_HEIGHT / 20, screen::SCREEN_HEIGHT * 3 / 20, WHITE);

				DrawText(&categ[static_cast<int>(CreditIndex::LIBRARY)].tx[0], categ[static_cast<int>(CreditIndex::LIBRARY)].pos.x, categ[static_cast<int>(CreditIndex::LIBRARY)].pos.y, categ[static_cast<int>(CreditIndex::LIBRARY)].size, categ[static_cast<int>(CreditIndex::LIBRARY)].color);
				DrawText(&categ[static_cast<int>(CreditIndex::ARTIST)].tx[0], categ[static_cast<int>(CreditIndex::ARTIST)].pos.x, categ[static_cast<int>(CreditIndex::ARTIST)].pos.y, categ[static_cast<int>(CreditIndex::ARTIST)].size, categ[static_cast<int>(CreditIndex::ARTIST)].color);
				DrawText(&categ[static_cast<int>(CreditIndex::DEVELOPER)].tx[0], categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.x, categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.y, categ[static_cast<int>(CreditIndex::DEVELOPER)].size, categ[static_cast<int>(CreditIndex::DEVELOPER)].color);

				DrawTexture(button.TX, button.size.x, button.size.y, WHITE);
				DrawText("MENU", button.tx.pos.x, button.tx.pos.y, button.tx.size, button.color);

				EndDrawing();

			}
			void deinit() {
				UnloadTexture(logo);
				UnloadTexture(button.TX);
			}

		}

	}

}