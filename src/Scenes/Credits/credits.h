#pragma once
#include "raylib.h"
#include "Extern Vars/Externs.h"

using namespace std;

namespace arca {

	namespace config {

		namespace credits {

			enum class AudioSelector {
				MENU = 0,
				LVL1,
				LVL2,
				LVL3,
				//LVL4,
				//LVL5,
				//LVL6,
				//LVL7,
				//LVL8,
				//LVL9,
				//LVL10
			};

			extern AudioSelector index;

			enum class CreditIndex {
				AUDIO,
				ARTIST,
				LIBRARY,
				DEVELOPER
			};

			extern CreditIndex cIndex;

			struct Button {

				Color color = WHITE;
				Rectangle size;
				Text tx;
				bool selected = false;
				Texture2D TX;

			};

			extern Button button;

			extern Texture2D logo;

			extern Text artist;
			extern Text lib;
			extern Text dev;
			extern Text audio[HowManyLvls];
			extern Text categ[4];

			extern bool continueInGame;

			extern void init();
			extern void update();
			extern void draw();
			extern void deinit();

		}

	}

}