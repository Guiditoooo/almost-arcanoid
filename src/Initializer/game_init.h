#pragma once
#include "raylib.h"
#include "Extern Vars/Externs.h"




namespace arca {

	namespace general {

		using namespace config;

		void generalInit();
		void sceneManager(Scenes&, Scenes);

		void runGame();

	}

}