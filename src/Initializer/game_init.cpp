#include "game_init.h"
#include "Scenes/Menus/menus.h"
#include "Scenes/Gameplay/game/game.h"
#include "Scenes/Credits/credits.h"
#include <iostream>

namespace arca {

	namespace general {

		using namespace config;

		void runGame() {

			generalInit();

			do {

				sceneManager(scene, nextScene);

				switch (scene) {

					case arca::config::Scenes::MENU:
						menu::update();
						menu::draw();
						break;
					case arca::config::Scenes::GAME:
						game::update();
						game::draw();
						break;
					case arca::config::Scenes::OPTIONS:
						menu::update();//para que no crashee
						menu::draw();
						break;
					case arca::config::Scenes::CREDITS:
						credits::update();
						credits::draw();
						break;
					case arca::config::Scenes::QUIT:
						menu::continueInGame = false;
						break;
					case arca::config::Scenes::FINISH:
						break;

				}

			} while (!WindowShouldClose() && menu::continueInGame);

		}

		void generalInit() {
			
			InitWindow(screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT, "ARKUNGLE!");
			SetTargetFPS(screen::FPS_RATE);			
			
		}

		void sceneManager(Scenes& scene, Scenes nextScene) {

			if (scene != nextScene) {

				switch (scene) {
				case arca::config::Scenes::GAME:
					game::deinit();
					break;
				case arca::config::Scenes::OPTIONS:
					menu::deinit();
					break;
				case arca::config::Scenes::CREDITS:
					credits::deinit();
					break;
				case arca::config::Scenes::FINISH:
					break;
				case arca::config::Scenes::MENU:
					menu::deinit();
					break;
				}

				switch (nextScene) {
				case arca::config::Scenes::GAME:
					game::init();
					break;
				case arca::config::Scenes::OPTIONS:
					break;
					menu::init();
				case arca::config::Scenes::CREDITS:
					credits::init();
					break;
				case arca::config::Scenes::FINISH:
					break;
				case arca::config::Scenes::MENU:
					menu::init();
					break;
				}

				scene = nextScene;

			}

		}


	}

}