#include "Externs.h"

namespace arca {

	namespace game_objects {

		namespace blocks {

			Blocks brick1[MAX_BLOCKS_X][MAX_BLOCKS_Y];
			Blocks brick2[MAX_BLOCKS_X][MAX_BLOCKS_Y];
			Blocks brick3[MAX_BLOCKS_X][MAX_BLOCKS_Y];
		
		}
	
	}

	namespace screen {

		Vector2 size[3] = { {600,800}, {800,800}, {1000,800} };

		WindowSize WS = WindowSize::BIG;

		float SCREEN_WIDTH = size[static_cast<int>(WS)].x;
		float SCREEN_HEIGHT = size[static_cast<int>(WS)].y;

		int FPS_RATE = 60;
	
	}

	namespace config {

		Scenes scene = Scenes::NONE;
		Scenes nextScene = Scenes::MENU;

	}

}