#pragma once
#include "raylib.h"
#include <iostream>

using namespace std;

namespace arca {

	const int HowManyLvls = 4;

	struct Text {

		string tx;
		Vector2 pos;
		int size;
		Color color = WHITE;

	};

	namespace screen {

		enum class WindowSize { SMALL, MEDIUM, BIG };

		extern WindowSize WS;
		extern Vector2 size[3];

		extern float SCREEN_WIDTH;
		extern float SCREEN_HEIGHT;

		extern int FPS_RATE;

	}
	
	namespace game_objects {

		struct Ball {
			float rad = 6.0f;
			float min_rad = 6.0f;
			float speed = 2.5f;
			bool visible = false;
			bool stopped = true;
			Vector2 pos;
			Color color;
			float dirX = 1;
			float dirY = -1;
		};

		struct Pad {

			float width = screen::SCREEN_WIDTH * 11 / 80;
			float height = screen::SCREEN_HEIGHT * 1 / 40;
			float speed = 0.8f;
			Vector2 pos = {screen::SCREEN_WIDTH/2-width/2, screen::SCREEN_HEIGHT * 27 / 40 - height/2};
			Color color = BLACK;

		};
	
		struct Game_area {

			const float WIDTH = screen::SCREEN_WIDTH * 18 / 20;
			const float HEIGHT = screen::SCREEN_HEIGHT * 13 / 20;
			const Vector2 MIN = { screen::SCREEN_WIDTH / 20 , screen::SCREEN_HEIGHT / 20 };
			const Vector2 MAX = { screen::SCREEN_WIDTH * 19 /20 , screen::SCREEN_HEIGHT * 14 / 20 };

			Color color = { 224, 143, 82, 255 };

		};
	
		struct Score_area {

			const float WIDTH = screen::SCREEN_WIDTH * 18 / 20;
			const float HEIGHT = screen::SCREEN_HEIGHT * 4 / 20;
			const Vector2 MIN = { screen::SCREEN_WIDTH / 20 , screen::SCREEN_HEIGHT * 15 / 20 };
			//const Vector2 MAX = { screen::SCREEN_WIDTH - config::border , screen::SCREEN_HEIGHT - config::border };

			Color color = { 224, 143, 82, 255 };

		};

		namespace blocks {

			const int MAX_BLOCKS_X = 5;
			const int MAX_BLOCKS_Y = 5;

			enum class BlockType {
				BREAKEABLE,
				INBREKEABLE,
				NONE
			};

			struct Blocks {
				Texture2D TX;
				Vector2 pos;
				Color color;
				BlockType type = BlockType::NONE;
			};

			extern Blocks brick1[MAX_BLOCKS_X][MAX_BLOCKS_Y];
			extern Blocks brick2[MAX_BLOCKS_X][MAX_BLOCKS_Y];
			extern Blocks brick3[MAX_BLOCKS_X][MAX_BLOCKS_Y];

		}

	}
	
	namespace config {

		enum class Scenes {
			GAME = 1,
			OPTIONS,
			CREDITS,
			QUIT,
			FINISH,
			MENU = 0,
			NONE = 99
		};

		extern Scenes scene;
		extern Scenes nextScene;

	}

}